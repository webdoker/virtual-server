FROM php:fpm
# добавь ниже сколько угодно пакетов для php
RUN apt-get update
RUN docker-php-ext-install pdo pdo_mysql
RUN pecl install xdebug  && docker-php-ext-enable xdebug
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
